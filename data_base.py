import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from flask import g
from psycopg2.extras import  NamedTupleCursor


def create_connection():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost",
                                dbname="aggregator",
                                user="agg",
                                password="password",
                                port=5432)

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    except Exception as inst:
        print("I am unable to connect to the database")
        print(inst)

    return conn

def create_tables(conn) -> None:
    requete = """CREATE TABLE IF NOT EXISTS articles (
        id serial, 
        title varchar(250), 
        summary varchar(500), 
        link varchar(500), 
        published  date,
        published_parsed date,
        author varchar(50), 
        authors_profession varchar(50));"""

    query_create_select(conn, requete)

    requete = """CREATE TABLE IF NOT EXISTS sources (
        id serial, 
        title varchar(250), 
        link varchar(500));"""

    query_create_select(conn, requete)
    
    requete = """CREATE TABLE IF NOT EXISTS Users (
        id_user CHAR (36) NOT NULL,
        prenom VARCHAR (30) NOT NULL,
        nom VARCHAR (30) NOT NULL,
        e_mail VARCHAR (50) NOT NULL,
        mot_de_passe_hash VARCHAR (256) NOT NULL,
        PRIMARY KEY (id_user));"""
	
    query_create_select(conn, requete)
    
    requete = """CREATE TABLE IF NOT EXISTS Adresse (id_adresse VARCHAR (36) NOT NULL,
        numero_rue VARCHAR (5) NOT NULL,
        nom_rue VARCHAR (50) NOT NULL,
        ville VARCHAR (50) NOT NULL,
        code_postal VARCHAR (7) NOT NULL,
        id_user VARCHAR (36) NOT NULL,
        PRIMARY KEY (id_adresse),
        FOREIGN KEY (id_user)
        REFERENCES Users (id_user)
        ON UPDATE CASCADE ON DELETE NO ACTION);"""


    query_create_select(conn, requete)


def query_create_select(conn, requete):
    cur = conn.cursor()
    cur.execute(requete)
    return cur

def query(conn, requete, data_dict):
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute(requete, data_dict)
    return cur

if __name__ == "__main__":
    with create_connection() as conn:
        try:
            create_tables(conn)
        except Exception as e:
            print(e)
        finally:
            conn.commit()   
        