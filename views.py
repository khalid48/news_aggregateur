
import data_base
from flask import Flask, request, render_template, flash, redirect, url_for, session,jsonify
import hashlib, uuid, os
import data_base
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from werkzeug.utils import secure_filename
import webbrowser
import time


app = Flask(__name__)


app.config.update(
    DEBUG=True,
    SECRET_KEY='secret_key'
)

@app.route('/')
def index():
    
    return render_template("index.html")


@app.route('/page_connect2')
def login():
	return render_template("page_connect2.html")  
@app.route('/register')
def register():
	return render_template("register.html")  




 
@app.route('/accueil')
def accueil():
    # On crée la connection à la base de données
    conn = data_base.create_connection()
    data_base.create_tables(conn)

     # On va chercher les sources
    requete_sources = "Select title, link From sources;"
    sources_dict = {'title': None, 'link': None}

    cursor_sources = data_base.query(conn, requete_sources, sources_dict)
    columns_sources = [column[0] for column in cursor_sources.description]

    print(cursor_sources.description)
    results_sources = []

    lignes_sources = cursor_sources.fetchall();

    print(lignes_sources)

    for row in cursor_sources.fetchall():
        print("Sources: ", row)
        results_sources.append(dict(zip(columns_sources, row)))

    print(results_sources)

    # On va chercher les articles
    requete_articles = "Select title, link, published, authors_profession From articles;"
    article_dict = {'title': None, 'link': None, 'published': None, 'authors_profession': None}

    cursor = data_base.query(conn, requete_articles, article_dict)
    columns = [column[0] for column in cursor.description]

    results_articles = []
    for row in cursor.fetchall():
        print(row)
        results_articles.append(dict(zip(columns, row)))


    conn.close()
    return render_template('accueil.html', titre="Sources !", mots=results_articles, sources=results_sources)


    #data = [{'title': 'Why does SQL still matters in 2020?', 'link': 'https://blog.timescale.com/blog/why-sql-beating-nosql-what-this-means-for-future-of-data-time-series-database-348b777b847a/'},
    #        {'title': 'Python made my day', 'link': 'https://devrant.com/rants/87714/made-my-day'},
    #        {'title': 'Checking for data issues', 'link': 'https://en.wikipedia.org/wiki/Data_validation'},
    #        {'title': 'Test google', 'link': 'https://www.google.fr/search?sxsrf=ALeKk02_aNgahsvvBKiOjO9IqIh5LExe2Q%3A1584295826464&source=hp&ei=km9uXtOcGpyajLsPuuar0Ag&q=test+google&oq=test+google&gs_l=psy-ab.3..0l10.1420.4063..4543...1.0..0.89.666.11......0....1..gws-wiz.......35i39j0i131j0i67j0i131i67.EbU-IUpJ2BM&ved=0ahUKEwjTrdDviZ3oAhUcDWMBHTrzCooQ4dUDCAU&uact=5'}]
@app.route('/search',methods=['GET'])
def search():
    quary=request.args.get('text')
    # On crée la connection à la base de données
    conn = data_base.create_connection()
    data_base.create_tables(conn)

     # On va chercher les sources
    requete_sources = "Select title, link From sources WHERE title like '%'+ quary+'%';"
    sources_dict = {'title': None, 'link': None}

    cursor_sources = data_base.query(conn, requete_sources, sources_dict)
    columns_sources = [column[0] for column in cursor_sources.description]

    print(cursor_sources.description)
    results_sources = []

    lignes_sources = cursor_sources.fetchall();

    print(lignes_sources)

    for row in cursor_sources.fetchall():
        print("Sources: ", row)
        results_sources.append(dict(zip(columns_sources, row)))

    print(results_sources)

    # On va chercher les articles
    requete_articles = "Select title, link, published, authors_profession From articles;"
    article_dict = {'title': None, 'link': None, 'published': None, 'authors_profession': None}

    cursor = data_base.query(conn, requete_articles, article_dict)
    columns = [column[0] for column in cursor.description]

    results_articles = []
    for row in cursor.fetchall():
        print(row)
        results_articles.append(dict(zip(columns, row)))


    conn.close()
    if len(results)>0:
        return results [0][1]
    else:
        return "no results found"


if __name__ == "__main__":
    app.run(host='localhost', debug=True)   
 


