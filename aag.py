import feedparser
import requests
import calendar
from time import strptime
from pprint import pprint
import data_base
 
# Function to fetch the rss feed and return the parsed RSS
def parseRSS( rss_url ):
    return feedparser.parse( rss_url )

# function that add quote before and after the value argument
def quote_sql_string(value):
    '''
    If `value` is a string type, escapes single quotes in the string
    and returns the string enclosed in single quotes.
    '''
    new_value = str(value)
    new_value = new_value.replace("'", "''")
    return "'{}'".format(new_value)

# Function grabs the rss feed headlines (titles) and returns them as a list
def getHeadlines( rss_url ):
    global detail_auteur
    headlines = []

    feed = parseRSS(rss_url)

    print(feed.keys())
    taille = 0
    months = dict(Jan="01", Feb="02", Mar="03", Apr="04", May="05", Jun="0", Jui="0", Aug="0", Sep="0", Oct="0", Nov="0", Dec="0")

    article_dict = {}

    for champs in feed.entries:
        print(champs.keys())
        print("Titre: ", champs.title)
        if len(champs.title) > taille:
            taille = len(champs.summary_detail)
        print(champs.summary)
        print("published: ", champs.published)
        date_importee = champs.published.split(" ")
        date_article = date_importee[1] + "/" + months[date_importee[2]] + "/" + date_importee[3]
        print("Mois: ", months[date_importee[2]])
        print("date_article: ", date_article)
        print("Auteur: ", champs.author)
        print("Auteur Details: ", champs.author_detail)
        # for chp_details in champs.title_detail:
        #     print(chp_details)
        for chp_links in champs.links:
            for chps in chp_links:
                print("chps: ", chps)

        print("Auteur: ", champs.author_detail)
        print("Id: ", champs.id)
        lien = champs.id
        print("guidislink: ", champs.guidislink)
        print("tags: ", champs.tags)

        requete ="Insert Into articles (title, summary, link, published, author, authors_profession) " \
                 "VALUES (%(title)s, %(summary)s, %(link)s, %(published)s, %(author)s, %(author_name)s);"
 #       for key, value in champs.author_detail.items():
 #           print("Infos auteur: ", key, value)
 #           detail_auteur = value

        lst_dictionnaire = []

        for key in champs.links:
            print("isinstance: ", isinstance(key, dict), key)
            print("après isinstance: ", key)
            if isinstance(key, dict):
                for key2, value in key.items():
                    if key2 == "href":
                  #      lien = value
                        print(key2, value)

        print("lien: ", lien)
        print(requete)

        article_dict['title'] = champs.title
        article_dict['summary'] = champs.summary
        article_dict['link'] = champs.link
        article_dict['published'] = champs.published
        article_dict['author'] = champs.author
        article_dict['author_name'] = champs.author_detail.name

        lst_dictionnaire.append(article_dict)
        print(article_dict)

        # METTRE LES VALEURS DES CHAMPS DANS UN DICTIONNAIRE

        data_base.query(conn, requete, article_dict)
        #data_base.query(conn, requete, lst_dictionnaire)

    print("taille max: ", taille)
    return headlines
 
# A list to hold all headlines
allheadlines = []
 
# List of RSS feeds that we will fetch and combine
newsurls = {
    'futura':             'https://www.futura-sciences.com/flux-rss/',

    'futura_science':    'https://www.futura-sciences.com/rss/actualites.xml',
    'futura_high_tech':   'https://www.futura-sciences.com/rss/high-tech/actualites.xml'
}

conn = data_base.create_connection()

# Iterate over the feed urls
for key,url in newsurls.items():
    # Call getHeadlines() and combine the returned headlines with allheadlines
    allheadlines.extend( getHeadlines( url ) )
 
 
# Iterate over the allheadlines list and print each headline
for hl in allheadlines:
    print(hl)
 
